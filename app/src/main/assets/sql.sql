
-- Table: words
CREATE TABLE words ( 
    _id         INTEGER PRIMARY KEY
                        UNIQUE,
    name        TEXT,
    translate   TEXT,
    type        INTEGER DEFAULT '0',
    association BLOB 
);

INSERT INTO [words] ([_id], [name], [translate], [type], [association]) VALUES (1, 'deutsch', 'немецкий', 5, '');
INSERT INTO [words] ([_id], [name], [translate], [type], [association]) VALUES (2, 'Elektroherd', 'электроплита', 0, '');
INSERT INTO [words] ([_id], [name], [translate], [type], [association]) VALUES (3, 'Tisch', 'стол', 0, '');
INSERT INTO [words] ([_id], [name], [translate], [type], [association]) VALUES (4, 'Foto', 'фото', 2, '');
