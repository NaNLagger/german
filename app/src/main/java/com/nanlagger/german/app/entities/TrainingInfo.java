package com.nanlagger.german.app.entities;

import com.orm.SugarRecord;
import com.orm.dsl.Table;

/**
 * Created by Stepan on 07.03.2016.
 */
@Table(name = "trainings")
public class TrainingInfo extends SugarRecord {

    private Long idWord;
    private int idTraining;
    private Long time;

    public TrainingInfo(Long idWord, int idTraining, Long time) {
        this.idWord = idWord;
        this.idTraining = idTraining;
        this.time = time;
    }

    public Long getIdWord() {
        return idWord;
    }

    public int getIdTraining() {
        return idTraining;
    }

    public Long getTime() {
        return time;
    }

    public void setIdWord(Long idWord) {
        this.idWord = idWord;
    }

    public void setIdTraining(int idTraining) {
        this.idTraining = idTraining;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
