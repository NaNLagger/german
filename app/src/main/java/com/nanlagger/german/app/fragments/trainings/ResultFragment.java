package com.nanlagger.german.app.fragments.trainings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.StartActivity;

/**
 * Created by Stepan on 21.02.2016.
 */
public class ResultFragment extends Fragment {

    private static final String EXTRA_RIGHT = "right";
    private static final String EXTRA_COUNT = "count";
    private static final String EXTRA_TRAINING = "id";

    private int rightAnswers;
    private int countAnswers;
    private int trainingId;

    private TextView resTextView;
    private TextView countTextView;

    public static ResultFragment newInstance(int right, int count, int trainingID) {
        ResultFragment f = new ResultFragment();
        Bundle bdl = new Bundle(1);
        bdl.putInt(EXTRA_RIGHT, right);
        bdl.putInt(EXTRA_COUNT, count);
        bdl.putInt(EXTRA_TRAINING, trainingID);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.result_adapter, container, false);
        setHasOptionsMenu(true);

        if (savedInstanceState == null) {
            rightAnswers = getArguments().getInt(EXTRA_RIGHT);
            countAnswers = getArguments().getInt(EXTRA_COUNT);
            trainingId = getArguments().getInt(EXTRA_TRAINING);
        } else {
            rightAnswers = savedInstanceState.getInt(EXTRA_RIGHT);
            countAnswers = savedInstanceState.getInt(EXTRA_COUNT);
            trainingId = savedInstanceState.getInt(EXTRA_TRAINING);
        }

        resTextView = (TextView) rootView.findViewById(R.id.resTextView);
        countTextView = (TextView) rootView.findViewById(R.id.countTextView);

        resTextView.setText(getRes());
        countTextView.setText("Правильных ответов: " + rightAnswers + " из " + countAnswers);

        Button tryAgain = (Button) rootView.findViewById(R.id.tryAgainButton);
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StartActivity) getActivity()).switchFragment(TrainingFragment.newInstance(trainingId), "training");
            }
        });
        Button backButton = (Button) rootView.findViewById(R.id.backToTrainingButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StartActivity) getActivity()).onBackPressed();
            }
        });
        return rootView;
    }

    public void Update(int right, int count) {
        rightAnswers = right;
        countAnswers = count;
        resTextView.setText(getRes());
        countTextView.setText("Правильных ответов: " + rightAnswers + " из " + countAnswers);
    }

    private String getRes() {
        float pRes = (float) rightAnswers / (float) countAnswers;
        String result = "Nya";
        if(pRes > 0.3) {
            result = "Soso lala";
        }
        if(pRes > 0.5) {
            result = "Good";
        }
        if(pRes > 0.8) {
            result = "Exelent";
        }
        if(rightAnswers == countAnswers) {
            result = "God Level";
        }
        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_RIGHT, rightAnswers);
        outState.putInt(EXTRA_COUNT, countAnswers);
        outState.putInt(EXTRA_TRAINING, trainingId);
    }
}
