package com.nanlagger.german.app.fragments.trainings;

import android.support.v4.app.Fragment;
import com.nanlagger.german.app.entities.TrainingInfo;
import com.nanlagger.german.app.entities.Word;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Stepan on 24.02.2016.
 */
abstract public class TrainingController {

    public class BlockWords {
        public Word rightWord;
        public ArrayList<Word> answers = new ArrayList<>();
        public boolean flagRes = false;
        public boolean result = false;

        public BlockWords(Word word) {
            rightWord = word;
        }

        public void addAnswer(Word word) {
            answers.add(word);
        }
    }

    protected int idTraining;
    protected ArrayList<BlockWords> blocks = new ArrayList<>();

    public void generateTest() {
        clear();
    }

    private void clear() {
        blocks.clear();
    }

    public BlockWords getBlock(int index) {
        return blocks.get(index);
    }

    public int getSize() {
        return blocks.size();
    }

    public void setResult(int index, boolean res) {
        if(blocks.get(index).flagRes)
            return;
        blocks.get(index).flagRes = true;
        blocks.get(index).result = res;
        long unixTime = System.currentTimeMillis() / 1000L;
        if(res) {
            blocks.get(index).rightWord.setProgress(blocks.get(index).rightWord.getProgress() + 10);
        } else {
            blocks.get(index).rightWord.setProgress(blocks.get(index).rightWord.getProgress() - 5);
        }
        blocks.get(index).rightWord.save();
        List<TrainingInfo> trainingInfoList = TrainingInfo.find(
                TrainingInfo.class, "ID_WORD = ? and ID_TRAINING = ?",
                blocks.get(index).rightWord.getId() + "",
                idTraining+"");
        TrainingInfo trainingInfo;
        if(trainingInfoList.size() == 0)
            trainingInfo = new TrainingInfo(blocks.get(index).rightWord.getId(), idTraining, unixTime);
        else
            trainingInfo = trainingInfoList.get(0);
        trainingInfo.setTime(unixTime);
        trainingInfo.save();
    }

    public boolean swapToPage(int index) {
        return (index < blocks.size() && blocks.get(index).flagRes) || (index - 1 >= 0 && blocks.get(index - 1).flagRes);
    }

    public int getRight() {
        int res = 0;
        for (BlockWords bl : blocks) {
            if (bl.flagRes && bl.result)
                res++;
        }
        return res;
    }

    abstract public Fragment getNewInstanceFragment(int position);

    public int getIdTraining() {
        return idTraining;
    }

    public void setIdTraining(int idTraining) {
        this.idTraining = idTraining;
    }
}
