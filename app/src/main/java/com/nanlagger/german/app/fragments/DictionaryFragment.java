package com.nanlagger.german.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.adapters.DictionaryAdapter;
import com.nanlagger.german.app.controllers.DictionaryControl;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.dialogs.EditWordDialog;
import com.nanlagger.german.app.fragments.dialogs.NoticeDialogListener;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by NaNLagger on 31.12.14.
 *
 * @author Stepan Lyashenko
 */
public class DictionaryFragment extends Fragment  implements NoticeDialogListener {
    DictionaryControl controller = null;
    boolean isChecked = false;
    DictionaryAdapter adapter;
    ArrayList<Word> entity = new ArrayList<Word>();

    public DictionaryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dictionary_fragment_layout, container, false);
        controller = new DictionaryControl((StartActivity) getActivity());

        ListView listView = (ListView) rootView.findViewById(R.id.dictionaryListView);
        listView.setFocusable(false);
        final EditText searchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
        updateEntity();
        adapter = new DictionaryAdapter(this, getActivity(), entity);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
        setHasOptionsMenu(true);

        ImageButton searchButton = (ImageButton) rootView.findViewById(R.id.searchImageButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entity.clear();
                entity.addAll(controller.changeList(searchEditText.getText().toString()));
                adapter.notifyDataSetChanged();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                entity.clear();
                entity.addAll(controller.changeList(searchEditText.getText().toString()));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        setupUI(rootView.findViewById(R.id.parentDictionary));
        return rootView;
    }

    public void setupUI(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((StartActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_section_dictionary));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.dictionary, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add:
                controller.actionAdd();
                break;
            case R.id.action_edit:
                isChecked = !isChecked;
                adapter.updateView(isChecked);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateEntity() {
        entity.clear();
        entity.addAll(Select.from(Word.class).where(Condition.prop("IS_ADDED").gt(0)).list());
    }

    public void editWordAction(Word word) {
        EditWordDialog dialog = EditWordDialog.newInstance(word.getId());
        dialog.setNoticeDialogListener(this);
        dialog.show(getFragmentManager(), "edit");
    }

    public void deleteWordAction(Word word) {
        word.delete();
        updateEntity();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        dialog.dismiss();
        updateEntity();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
