package com.nanlagger.german.app.controllers;

import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.entities.Word;

import java.util.ArrayList;

/**
 * Created by NaNLagger on 13.01.15.
 *
 * @author Stepan Lyashenko
 */
public class AddingWordsControl {

    private final StartActivity activity;

    public AddingWordsControl(StartActivity activity) {
        this.activity = activity;
    }

    public ArrayList<Word> changeList(String text) {
        return new ArrayList<>(Word.find(Word.class, "upper(NAME) LIKE upper('"+text+"%')"));
    }
}
