package com.nanlagger.german.app.fragments.trainings.constructorword;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.untils.views.FlowLayout;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Stepan on 26.02.2016.
 */
public class ConstructorWordSlideFragment extends Fragment {
    private static final String EXTRA_ID = "id";
    private static final String EXTRA_CHARINDEX = "charindex";
    private ConstructorWordController.BlockWords block;
    ArrayList<Button> buttons = new ArrayList<Button>();
    ArrayList<TextView> rightButtons = new ArrayList<>();
    private int idSlide;
    private int currentChar = 0;

    public static ConstructorWordSlideFragment getNewInstance(int id) {
        ConstructorWordSlideFragment f = new ConstructorWordSlideFragment();
        Bundle bdl = new Bundle(1);
        bdl.putInt(EXTRA_ID, id);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.constructor_word_adapter, container, false);

        if (savedInstanceState == null) {
            idSlide = getArguments().getInt(EXTRA_ID);
            currentChar = 0;
        } else {
            currentChar = savedInstanceState.getInt(EXTRA_CHARINDEX);
            idSlide = savedInstanceState.getInt(EXTRA_ID);
        }
        block = ConstructorWordController.getInstance().getBlock(idSlide);

        TextView qTextView = (TextView) rootView.findViewById(R.id.qTextView);

        qTextView.setText(block.rightWord.getTranslate());

        final FlowLayout flowLayout = (FlowLayout) rootView.findViewById(R.id.buttonsContainer);
        ArrayList<Character> characters = new ArrayList<>();
        for (char z : block.rightWord.getName().toCharArray()) {
            characters.add(z);
        }
        Random random = new Random(System.currentTimeMillis());
        for (char z : block.rightWord.getName().toCharArray()) {
            int index = Math.abs(random.nextInt())%characters.size();
            final Button button = new Button(getActivity());
            button.setText(characters.get(index) + "");
            characters.remove(index);
            button.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(block.rightWord.getName().charAt(currentChar) == button.getText().charAt(0)) {
                        v.setVisibility(View.INVISIBLE);
                        rightButtons.get(currentChar).setVisibility(View.VISIBLE);
                        currentChar++;
                        updateButtons();
                        if(currentChar == block.rightWord.getName().length()) {
                            ConstructorWordController.getInstance().setResult(idSlide, true);
                        }
                    } else {
                        v.setBackgroundColor(Color.RED);
                        ConstructorWordController.getInstance().setResult(idSlide, false);
                        rightButtons.get(currentChar).setBackgroundColor(Color.RED);
                    }
                }
            });
            flowLayout.addView(button);
            buttons.add(button);
        }

        final LinearLayout linearLayout1 = (LinearLayout) rootView.findViewById(R.id.rightAnswerContainer);
        for (int i = 0; i < block.rightWord.getName().length(); i++) {
            final TextView button = new TextView(getActivity());
            button.setTextSize(22);
            button.setTextColor(Color.BLACK);
            button.setText(block.rightWord.getName().charAt(i) + "");
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            //params.setMargins(0,0,5,0);
            button.setLayoutParams(params);
            if(i < currentChar)
                button.setVisibility(View.VISIBLE);
            else
                button.setVisibility(View.INVISIBLE);
            button.setEnabled(false);
            linearLayout1.addView(button);
            rightButtons.add(button);
        }
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EXTRA_ID, idSlide);
        outState.putInt(EXTRA_CHARINDEX, currentChar);
    }

    public void updateButtons() {
        for (Button button : buttons) {
            button.setBackgroundColor(Color.WHITE);
        }
    }
}
