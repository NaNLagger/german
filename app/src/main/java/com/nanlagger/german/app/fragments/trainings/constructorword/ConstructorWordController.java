package com.nanlagger.german.app.fragments.trainings.constructorword;

import android.support.v4.app.Fragment;
import com.nanlagger.german.app.entities.Training;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.trainings.TrainingController;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Stepan on 26.02.2016.
 */
public class ConstructorWordController extends TrainingController {

    private static ConstructorWordController instance = null;

    private ConstructorWordController() {
        idTraining = Training.CONSTRUCTOR_WORDS.getIdTraining();
    }

    public static ConstructorWordController getInstance() {
        if(instance == null)
            instance = new ConstructorWordController();
        return instance;
    }

    @Override
    public Fragment getNewInstanceFragment(int position) {
        return ConstructorWordSlideFragment.getNewInstance(position);
    }

    @Override
    public void generateTest() {
        super.generateTest();

        long unixTime = System.currentTimeMillis() / 1000L;
        ArrayList<Word> rightW = new ArrayList<>(Word.find(Word.class,
                "PROGRESS < ? and ID NOT IN (SELECT ID_WORD FROM trainings WHERE ID_TRAINING = ? and TIME > ?)",
                new String[]{
                        Word.MAX_PROGRESS+"",
                        Training.CONSTRUCTOR_WORDS.getIdTraining()+"",
                        (unixTime - Training.CONSTRUCTOR_WORDS.getTimeout())+""
                }));
        Random random = new Random();
        for (int i = 0; i < 10 && rightW.size() > 0; i++) {
            BlockWords blockWords;

            int z = Math.abs(random.nextInt()) % rightW.size();
            Word right = rightW.get(z);
            rightW.remove(z);
            blockWords = new BlockWords(right);
            blocks.add(blockWords);
        }
    }
}
