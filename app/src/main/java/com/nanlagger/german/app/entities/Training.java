package com.nanlagger.german.app.entities;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.nanlagger.german.app.fragments.trainings.TrainingController;
import com.nanlagger.german.app.fragments.trainings.constructorword.ConstructorWordController;
import com.nanlagger.german.app.fragments.trainings.translateword.TranslateWordController;
import com.nanlagger.german.app.fragments.trainings.wordtranslate.WordTranslateController;

/**
 * Created by Stepan on 23.02.2016.
 */
public enum Training {
    WORD_TRANSLATE(1, "Word-Translate", 300),
    TRANSLATE_WORD(2, "Translate-Word", 300),
    CONSTRUCTOR_WORDS(3, "Constructor words", 300),
    CARDS_WORDS(4, "Cards words", 300);

    private final int timeout;
    private final int idTraining;
    private final String nameTraining;

    Training(int idTraining, String nameTraining, int timeout) {
        this.idTraining = idTraining;
        this.nameTraining = nameTraining;
        this.timeout = timeout;
    }

    public int getIdTraining() {
        return idTraining;
    }

    public String getNameTraining() {
        return nameTraining;
    }

    public int getTimeout() {
        return timeout;
    }

    public static TrainingController getController(int idTraining) {
        Log.v("Training", idTraining + "");
        TrainingController trainingController;
        if(idTraining == WORD_TRANSLATE.idTraining)
            return WordTranslateController.getInstance();
        if(idTraining == TRANSLATE_WORD.idTraining)
            return TranslateWordController.getInstance();
        if(idTraining == CONSTRUCTOR_WORDS.idTraining)
            return ConstructorWordController.getInstance();
        return WordTranslateController.getInstance();
    }
}
