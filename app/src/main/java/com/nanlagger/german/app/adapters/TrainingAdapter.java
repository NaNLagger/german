package com.nanlagger.german.app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.entities.Training;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.trainings.TrainingFragment;

/**
 * Created by Stepan on 23.02.2016.
 */
public class TrainingAdapter extends RecyclerView.Adapter<TrainingAdapter.ViewHolder> {

    private Training[] trainingArrayList;
    private StartActivity startActivity;

    public TrainingAdapter(StartActivity activity, Training[] trainings) {
        startActivity = activity;
        trainingArrayList = trainings;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.training_blocks_adapter, viewGroup, false);

        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final Training training = trainingArrayList[i];

        LinearLayout linearLayout = viewHolder.linearLayout;
        ImageView iconImageView = viewHolder.iconImageView;
        TextView titleTextView = viewHolder.titleTextView;
        TextView countTextView = viewHolder.countTextView;

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity.switchFragment(TrainingFragment.newInstance(training.getIdTraining()), "training");
            }
        });
        titleTextView.setText(training.getNameTraining());
        long unixTime = System.currentTimeMillis() / 1000L;
        Long count = Word.count(
                Word.class,
                "PROGRESS < ? and ID NOT IN (SELECT ID_WORD FROM trainings WHERE ID_TRAINING = ? and TIME > ?)",
                new String[]{Word.MAX_PROGRESS+"", training.getIdTraining()+"", (unixTime - training.getTimeout())+""});
        countTextView.setText(count + " слова");
    }

    @Override
    public int getItemCount() {
        return trainingArrayList.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout linearLayout;
        public ImageView iconImageView;
        public TextView titleTextView;
        public TextView countTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            linearLayout = (LinearLayout) itemView.findViewById(R.id.trainingButton);
            iconImageView = (ImageView) itemView.findViewById(R.id.trainingImageView);
            titleTextView = (TextView) itemView.findViewById(R.id.trainingTitle);
            countTextView = (TextView) itemView.findViewById(R.id.countWordTextView);
        }
    }
}
