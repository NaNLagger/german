package com.nanlagger.german.app.fragments.dialogs;

import android.support.v4.app.DialogFragment;

/**
 * Created by NaNLagger on 14.01.15.
 *
 * @author Stepan Lyashenko
 */
public interface NoticeDialogListener {
    public void onDialogPositiveClick(DialogFragment dialog);
    public void onDialogNegativeClick(DialogFragment dialog);
}
