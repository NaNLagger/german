package com.nanlagger.german.app.fragments.trainings;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.entities.Training;

/**
 * Created by Stepan on 24.02.2016.
 */
public class TrainingFragment extends Fragment {

    private static final String PAGE_SELECTED = "page";
    private static final String EXTRA_ID = "id_training";

    private TrainingController controller;
    private int currentPage = 0;
    private int idTraining = 0;

    public static TrainingFragment newInstance(int idTraining) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_ID, idTraining);
        TrainingFragment fragment = new TrainingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.word_translate_training, container, false);
        setHasOptionsMenu(true);
        final ViewPager mPager = (ViewPager) rootView.findViewById(R.id.pager);
        if(savedInstanceState != null) {
            currentPage = savedInstanceState.getInt(PAGE_SELECTED);
            idTraining = savedInstanceState.getInt(EXTRA_ID);
        } else {
            idTraining = getArguments().getInt(EXTRA_ID);
        }
        controller = Training.getController(idTraining);
        if(savedInstanceState == null) {
            controller.generateTest();
        }
        final TrainingSlideAdapter mPagerAdapter = new TrainingSlideAdapter(getActivity().getSupportFragmentManager(), controller);
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(currentPage);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                if(i == controller.getSize())
                    mPagerAdapter.notifyDataSetChanged();
                if(currentPage <= i && controller.swapToPage(i)) {
                    currentPage = i;
                } else {
                    mPager.setCurrentItem(currentPage);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PAGE_SELECTED, currentPage);
        outState.putInt(EXTRA_ID, idTraining);
    }
}
