package com.nanlagger.german.app.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.adapters.TrainingAdapter;
import com.nanlagger.german.app.entities.Training;

/**
 * Created by NaNLagger on 31.12.14.
 *
 * @author Stepan Lyashenko
 */
public class TrainingMenuFragment extends Fragment {

    private Training[] trainings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.trainings_fragment_layout, container, false);
        RecyclerView rvTrainings = (RecyclerView) rootView.findViewById(R.id.trainingRecyclerView);

        trainings = Training.values();
        TrainingAdapter adapter = new TrainingAdapter((StartActivity) getActivity(), trainings);
        rvTrainings.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        else
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvTrainings.setLayoutManager(linearLayoutManager);
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((StartActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_section_training_menu));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
