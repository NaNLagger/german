package com.nanlagger.german.app.fragments.trainings;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Stepan on 24.02.2016.
 */
public class TrainingSlideAdapter extends FragmentStatePagerAdapter {

    private int count = 0;
    private ResultFragment resultFragment;
    private TrainingController controller;

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        try {
            resultFragment.Update(controller.getRight(), controller.getSize());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public TrainingSlideAdapter(FragmentManager fm, TrainingController ctrl) {
        super(fm);
        controller = ctrl;
        count = ctrl.getSize() + 1;
        resultFragment = ResultFragment.newInstance(controller.getRight(), controller.getSize(), controller.getIdTraining());
    }

    @Override
    public Fragment getItem(int i) {
        if(i == count - 1) {
            return resultFragment;
        }
        return controller.getNewInstanceFragment(i);
    }

    @Override
    public int getCount() {
        return count;
    }
}
