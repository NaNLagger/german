package com.nanlagger.german.app.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.*;
import android.widget.Button;
import android.widget.Toast;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.controllers.MainMenuControl;

/**
 * Created by NaNLagger on 31.12.14.
 *
 * @author Stepan Lyashenko
 */
public class MainMenuFragment extends Fragment {
    MainMenuControl controller = null;

    public MainMenuFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.menu_fragment_layout, container, false);
        controller = new MainMenuControl((StartActivity) getActivity());
        ((Button) rootView.findViewById(R.id.dictionaryButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.startDictionary();
            }
        });
        setHasOptionsMenu(true);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((StartActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_section_home));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.start, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
