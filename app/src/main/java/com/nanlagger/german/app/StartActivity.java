package com.nanlagger.german.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import com.nanlagger.german.app.fragments.DictionaryFragment;
import com.nanlagger.german.app.fragments.MainMenuFragment;
import com.nanlagger.german.app.fragments.NavigationDrawerFragment;
import com.nanlagger.german.app.fragments.TrainingMenuFragment;
import com.orm.SugarContext;


public class StartActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SugarContext.init(this);
        setContentView(R.layout.activity_start);
        getSupportActionBar().setElevation(0);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);

        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        switch (position) {
            case 0:
                switchFragment(new MainMenuFragment(), "mainmenu");
                break;
            case 1:
                switchFragment(new DictionaryFragment(), "dictionary");
                break;
            case 2:
                switchFragment(new TrainingMenuFragment(), "trainingmenu");
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() < 2) {
            finish();
        }
        else {
            String name = getSupportFragmentManager()
                    .getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 2)
                    .getName();
            if (name != null && name.equalsIgnoreCase("training")) {
                getSupportFragmentManager().popBackStack("trainingmenu", 0);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    public void switchFragment(Fragment fragment) {
        switchFragment(fragment, null);
    }

    public void switchFragment(Fragment fragment, String tag) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, fragment, tag)
                    .addToBackStack(tag)
                    .commit();
        }
    }
}
