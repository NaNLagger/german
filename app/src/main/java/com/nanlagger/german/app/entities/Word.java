package com.nanlagger.german.app.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;

import java.io.Serializable;

/**
 * Created by NaNLagger on 12.01.15.
 *
 * @author Stepan Lyashenko
 */
@Table(name = "words")
public class Word extends SugarRecord implements Parcelable {
    @Ignore
    public static final int MIN_PROGRESS = 0;
    @Ignore
    public static final int MAX_PROGRESS = 100;

    protected Word(Parcel in) {
        name = in.readString();
        translate = in.readString();
        pos = in.readString();
        gen = in.readString();
        progress = in.readInt();
    }

    public static final Creator<Word> CREATOR = new Creator<Word>() {
        @Override
        public Word createFromParcel(Parcel in) {
            return new Word(in);
        }

        @Override
        public Word[] newArray(int size) {
            return new Word[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(MIN_PROGRESS);
        dest.writeInt(MAX_PROGRESS);
        dest.writeString(name);
        dest.writeString(translate);
        dest.writeString(pos);
        dest.writeString(gen);
        dest.writeInt(progress);
    }

    public enum Pos {
        VERB("verb"), NOUN("noun"), ADJ("adjective");

        private String pos;

        Pos(String pos) {
            this.pos = pos;
        }

        public String getPos() {
            return pos;
        }
    }

    public enum Gen {
        M("m", "der"), N("n", "das"), F("f", "die"), PL("pl", "die");

        private String gen;
        private String parseGen;

        Gen(String gen, String parseGen) {
            this.gen = gen;
            this.parseGen = parseGen;
        }

        public String getGen() {
            return gen;
        }

        public String getParseGen() {
            return parseGen;
        }

        public static Gen getValueByGen(String value) {
            for (Gen genName: values()) {
                if(genName.getGen().equals(value))
                    return genName;
            }
            return null;
        }
    }

    protected String name;
    protected String translate;
    protected String pos;
    protected String gen;
    protected int progress;
    protected Drawable association;
    protected int isAdded = 0;

    /*Constructors*/
    public Word() {
        super();
        setId(-1L);
    }

    public Word(String name, String translate) {
        super();
        this.name = name;
        this.translate = translate;
    }

    public Word(String name, String translate, String pos) {
        this(name, translate);
        this.pos = pos;
    }

    /*Getters*/

    public String getName() {
        return name;
    }

    public String getParseName() {
        if(pos != null && pos.equalsIgnoreCase(Pos.NOUN.getPos())) {
            if(gen != null && Gen.getValueByGen(gen) != null)
                return Gen.getValueByGen(gen).getParseGen() + " " + name;
        }
        return name;
    }

    public String getTranslate() {
        return translate;
    }


    public int getProgress() {
        return progress;
    }

    public String getPos() {
        return pos;
    }

    public Gen getGen() {
        return Gen.getValueByGen(gen);
    }

    public Drawable getAssociation() {
        return association;
    }

    public boolean isAdded() {
        return isAdded > 0;
    }

    /*Setters*/
    public void setName(String name) {
        this.name = name;
    }

    public void setProgress(int progress) {
        this.progress = progress;
        if (this.progress < MIN_PROGRESS) {
            this.progress = MIN_PROGRESS;
        }
        if (this.progress > MAX_PROGRESS) {
            this.progress = MAX_PROGRESS;
        }
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    public void setAssociation(Drawable association) {
        this.association = association;
    }

    public void setGen(Gen gen) {
        if(gen != null)
            this.gen = gen.getGen();
    }

    public void setAdded(boolean added) {
        isAdded = added ? 1 : 0;
    }

    @Override
    public String toString() {
        return getId() + " | " + name + " | " + translate;
    }

}
