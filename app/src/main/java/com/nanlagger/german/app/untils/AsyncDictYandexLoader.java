package com.nanlagger.german.app.untils;

import android.os.AsyncTask;
import android.util.Log;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nanlagger.german.app.controllers.AsyncListener;
import com.nanlagger.german.app.entities.Word;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Stepan on 18.02.2016.
 */
public class AsyncDictYandexLoader extends AsyncTask<String, Integer, ArrayList<Word>> {

    String urlAPI = "https://dictionary.yandex.net/api/v1/dicservice.json/lookup?";
    String keyAPI = "dict.1.1.20160218T134544Z.39383788c176754d.ebe5f4ec5c1adb35078c3334ce602235aff0b02a";
    String[] lang = {"ru-de", "de-ru"};
    String text = "";
    String format = "plain";
    AsyncListener mListener;

    public AsyncDictYandexLoader(AsyncListener listener) {
        mListener = listener;
    }

    @Override
    protected ArrayList<Word> doInBackground(String... params) {
        ArrayList<Word> result = new ArrayList<Word>();
        String localResult = "";
        text = params[0];
        for (String currentLang : lang) {
            String url = urlAPI + "key=" + keyAPI + "&text=" + text + "&lang=" + currentLang;
            DefaultHttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(url);
            HttpResponse response = null;
            try {
                response = client.execute(request);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (response != null) {
                try {
                    localResult = EntityUtils.toString(response.getEntity());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                return result;
            }
            Log.v("Loader", localResult);
            JsonParser parser = new JsonParser();
            JsonObject mainObject = parser.parse(localResult).getAsJsonObject();

            if (mainObject != null) {
                JsonArray pText = mainObject.getAsJsonArray("def");
                if(pText.size() == 0)
                    continue;
                for (JsonElement element : pText) {
                    //Log.v("Loader", element.getAsString());
                    if(currentLang.equals(lang[1])) {
                        result.addAll(ParseDefElement(element.getAsJsonObject()));
                    } else {

                    }
                }
            } else {
                return result;
            }
        }
        return result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(ArrayList<Word> o) {
        mListener.getResult(o);
    }

    private ArrayList<Word> ParseDefElement(JsonObject element) {
        ArrayList<Word> resultList = new ArrayList<>();
        String textR = element.get("text").getAsString();
        String pos = "";
        String type = "";
        try {
            pos = element.get("pos").getAsString();
            type = element.get("gen").getAsString();
        } catch (NullPointerException e) {

        }
        JsonArray tr = element.get("tr").getAsJsonArray();
        for (JsonElement obj : tr) {
            Word word = new Word(textR, obj.getAsJsonObject().get("text").getAsString(), pos);
            word.setGen(Word.Gen.getValueByGen(type));
            resultList.add(word);
        }
        return resultList;
    }
}