package com.nanlagger.german.app.entities;

import com.google.common.base.Strings;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;

/**
 * Created by Stepan on 07.03.2016.
 */
@Table
public class VerbInfo {
    @Ignore
    public static final int VERB_HABEN = 0;
    @Ignore
    public static final int VERB_SEIN = 1;

    private Long id;
    private Long idWord;
    private Strings prateritum;
    private Strings partizip2;
    private int assist;

    public VerbInfo(Long id, Long idWord, Strings prateritum, Strings partizip2, int assist) {
        this.id = id;
        this.idWord = idWord;
        this.prateritum = prateritum;
        this.partizip2 = partizip2;
        this.assist = assist;
    }

    public int getAssist() {
        return assist;
    }

    public Strings getPartizip2() {
        return partizip2;
    }

    public Strings getPrateritum() {
        return prateritum;
    }

    public Long getIdWord() {
        return idWord;
    }

    public Long getId() {
        return id;
    }

    public void setIdWord(Long idWord) {
        this.idWord = idWord;
    }

    public void setPrateritum(Strings prateritum) {
        this.prateritum = prateritum;
    }

    public void setPartizip2(Strings partizip2) {
        this.partizip2 = partizip2;
    }

    public void setAssist(int assist) {
        this.assist = assist;
    }
}
