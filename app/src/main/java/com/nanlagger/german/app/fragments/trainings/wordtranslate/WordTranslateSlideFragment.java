package com.nanlagger.german.app.fragments.trainings.wordtranslate;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.nanlagger.german.app.R;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by NaNLagger on 21.01.15.
 * @author Stepan Lyashenko
 */

public class WordTranslateSlideFragment extends Fragment {

    private static final String EXTRA_ID = "id";
    private WordTranslateController.BlockWords block;
    ArrayList<Button> buttons = new ArrayList<Button>();
    private int idSlide;

    public static WordTranslateSlideFragment getNewInstance(int id) {
        WordTranslateSlideFragment f = new WordTranslateSlideFragment();
        Bundle bdl = new Bundle(1);
        bdl.putInt(EXTRA_ID, id);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.word_translate_adapter, container, false);

        idSlide = getArguments().getInt(EXTRA_ID);
        block = WordTranslateController.getInstance().getBlock(idSlide);

        final TextView aTextView = (TextView) rootView.findViewById(R.id.aTextView);
        TextView qTextView = (TextView) rootView.findViewById(R.id.qTextView);

        qTextView.setText(block.rightWord.getName());
        aTextView.setText(block.rightWord.getTranslate());
        if(!block.flagRes)
            aTextView.setVisibility(View.INVISIBLE);
        else
            aTextView.setVisibility(View.VISIBLE);

        buttons.add((Button) rootView.findViewById(R.id.button));
        buttons.add((Button) rootView.findViewById(R.id.button2));
        buttons.add((Button) rootView.findViewById(R.id.button3));
        buttons.add((Button) rootView.findViewById(R.id.button4));
        View.OnClickListener buttonListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button click = (Button) v;
                aTextView.setVisibility(View.VISIBLE);
                if(click.getText().toString().equalsIgnoreCase(block.rightWord.getTranslate())) {
                    click.setBackgroundColor(getResources().getColor(R.color.green));
                    blockButtons();
                    WordTranslateController.getInstance().setResult(idSlide, true);
                } else {
                    click.setBackgroundColor(getResources().getColor(R.color.red));
                    blockButtons();
                    WordTranslateController.getInstance().setResult(idSlide, false);
                }
            }
        };
        for (int i=0; i < buttons.size() && i < block.answers.size(); i++) {
            buttons.get(i).setText(block.answers.get(i).getTranslate());
            buttons.get(i).setOnClickListener(buttonListener);
        }
        Random random = new Random();
        int z = Math.abs(random.nextInt()) % 4;
        buttons.get(z).setText(block.rightWord.getTranslate());
        if(block.flagRes) {
            blockButtons();
            if(block.result)
                buttons.get(z).setBackgroundColor(getResources().getColor(R.color.green));
        }
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    public void blockButtons() {
        for (Button button : buttons) {
            button.setEnabled(false);
        }
    }
}