package com.nanlagger.german.app.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.entities.Word;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NaNLagger on 14.01.15.
 *
 * @author Stepan Lyashenko
 */
public class AddingWordsAdapter extends WordsAdapter {

    public AddingWordsAdapter(Context context, ArrayList<Word> entity) {
        super(context, entity);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.addingword_adapter_layout, parent, false);
        }
        final Word word = getWord(position);

        final TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        final TextView translateTextView = (TextView) view.findViewById(R.id.translateTextView);
        final ImageView stImageView = (ImageView) view.findViewById(R.id.stImageView);
        final ImageButton statusButton = (ImageButton) view.findViewById(R.id.statusImageButton);

        nameTextView.setText(word.getParseName());
        translateTextView.setText(word.getTranslate());

        checkWord(word, stImageView, statusButton);

        statusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                word.setAdded(true);
                word.save();
                checkWord(word, stImageView, statusButton);
            }
        });
        return view;
    }

    private void checkWord(Word word, ImageView stImageView, ImageButton statusButton) {
        if(word.getName() == null || word.getTranslate() == null)
            return;
        List<Word> words = Select.from(Word.class)
                .where(Condition.prop("NAME").eq(word.getName()))
                .and(Condition.prop("TRANSLATE").eq(word.getTranslate()))
                .and(Condition.prop("IS_ADDED").gt(0)).list();
        if(word.isAdded() || words.size() > 0) {
            statusButton.setVisibility(View.INVISIBLE);
            stImageView.setVisibility(View.VISIBLE);
        } else {
            statusButton.setVisibility(View.VISIBLE);
            stImageView.setVisibility(View.GONE);
        }
    }
}
