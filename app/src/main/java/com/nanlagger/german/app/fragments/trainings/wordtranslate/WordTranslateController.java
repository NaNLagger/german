package com.nanlagger.german.app.fragments.trainings.wordtranslate;

import android.support.v4.app.Fragment;
import com.nanlagger.german.app.entities.Training;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.trainings.TrainingController;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Stepan on 20.02.2016.
 */
public class WordTranslateController extends TrainingController{

    private static WordTranslateController instance = null;

    private WordTranslateController() {
        idTraining = Training.WORD_TRANSLATE.getIdTraining();
    }

    public static WordTranslateController getInstance() {
        if(instance == null)
            instance = new WordTranslateController();
        return instance;
    }

    @Override
    public void generateTest() {
        super.generateTest();

        long unixTime = System.currentTimeMillis() / 1000L;
        ArrayList<Word> rightW = new ArrayList<>(Word.find(Word.class,
                "progress < ? and id NOT IN (SELECT ID_WORD FROM trainings WHERE ID_TRAINING = ? and TIME > ?)",
                new String[]{
                        Word.MAX_PROGRESS+"",
                        Training.WORD_TRANSLATE.getIdTraining()+"",
                        (unixTime - Training.WORD_TRANSLATE.getTimeout())+""
                }));
        Random random = new Random();
        for (int i = 0; i < 10 && rightW.size() > 0; i++) {
            BlockWords blockWords;

            int z = Math.abs(random.nextInt()) % rightW.size();
            Word right = rightW.get(z);
            rightW.remove(z);
            blockWords = new BlockWords(right);

            ArrayList<Word> arrayAnswers = new ArrayList<>(Word.find(Word.class,
                    "progress < ? and POS = ? and ID NOT IN (SELECT ID_WORD FROM trainings WHERE ID_TRAINING = ? and TIME > ?) LIMIT ?",
                    new String[]{
                            (Word.MAX_PROGRESS + 1) + "",
                            right.getPos(),
                            Training.TRANSLATE_WORD.getIdTraining() + "",
                            (unixTime - Training.TRANSLATE_WORD.getTimeout()) + "",
                            400+""
                    }));
            for (int j = 0; j < 4 && arrayAnswers.size() > 0; j++) {
                int index = Math.abs(random.nextInt()) % arrayAnswers.size();
                blockWords.addAnswer(arrayAnswers.get(index));
                arrayAnswers.remove(index);
            }
            blocks.add(blockWords);
        }
    }

    @Override
    public Fragment getNewInstanceFragment(int position) {
        return WordTranslateSlideFragment.getNewInstance(position);
    }
}
