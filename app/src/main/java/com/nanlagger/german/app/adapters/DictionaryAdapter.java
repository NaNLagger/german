package com.nanlagger.german.app.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.DictionaryFragment;

import java.util.ArrayList;

/**
 * Created by NaNLagger on 12.01.15.
 *
 * @author Stepan Lyashenko
 */
public class DictionaryAdapter extends WordsAdapter {

    boolean flag = false;
    DictionaryFragment parent;

    public DictionaryAdapter(DictionaryFragment parent, Context context, ArrayList<Word> entity) {
        super(context, entity);
        this.parent = parent;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.dictionary_adapter_layout, parent, false);
        }
        final Word word = getWord(position);
        TextView idTextView = (TextView) view.findViewById(R.id.idTextView);
        TextView nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        TextView translateTextView = (TextView) view.findViewById(R.id.translateTextView);
        TextView progressTextView = (TextView) view.findViewById(R.id.progressTextView);
        ImageButton editButton = (ImageButton) view.findViewById(R.id.editImageButton);
        ImageButton deleteButton = (ImageButton) view.findViewById(R.id.deleteImageButton);

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DictionaryAdapter.this.parent.editWordAction(word);
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DictionaryAdapter.this.parent.deleteWordAction(word);
            }
        });

        idTextView.setText(String.valueOf(word.getId()));
        nameTextView.setText(word.getParseName());
        translateTextView.setText(word.getTranslate());
        showProgress(progressTextView, word.getProgress());
        if(flag) {
            editButton.setVisibility(View.VISIBLE);
            deleteButton.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.GONE);
            deleteButton.setVisibility(View.GONE);
        }
        return view;
    }

    public void updateView(boolean flag) {
        this.flag = flag;
        notifyDataSetChanged();
    }

    private void showProgress(TextView progressTextView, int progress) {
        progressTextView.setText(progress + "%");
        int r,g,b;
        if(progress <= 50) {
            r = 255;
            g = (int) (progress * ((float)255 / (float)50));
            b = (int) (progress * ((float)255 / (float)100));
        } else {
            r = 255 - (int) ((progress - 50) * ((float)255 / (float)50));
            g = 255;
            b = 255 - (int) (progress * ((float)255 / (float)100));
        }
        progressTextView.setBackgroundColor(Color.argb(255, r, g, b));
    }
}
