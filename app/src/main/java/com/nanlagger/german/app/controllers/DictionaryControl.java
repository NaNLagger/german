package com.nanlagger.german.app.controllers;

import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.AddingWordsFragment;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;

/**
 * Created by NaNLagger on 31.12.14.
 *
 * @author Stepan Lyashenko
 */
public class DictionaryControl {
    StartActivity activity;

    public DictionaryControl(StartActivity activity) {
        this.activity = activity;
    }

    public void actionAdd() {
        activity.switchFragment(new AddingWordsFragment());
    }

    public ArrayList<Word> changeList(String text) {
        return new ArrayList<>(Select.from(Word.class)
                .where(Condition.prop("upper(NAME)").like(text.toUpperCase() + "%"))
                .and(Condition.prop("IS_ADDED").gt(0)).list());
    }
}
