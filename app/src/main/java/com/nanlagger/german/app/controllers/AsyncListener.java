package com.nanlagger.german.app.controllers;

import com.nanlagger.german.app.entities.Word;

import java.util.ArrayList;

/**
 * Created by NaNLagger on 20.01.15.
 *
 * @author Stepan Lyashenko
 */
public interface AsyncListener {
    public void getResult(ArrayList<Word> result);
}
