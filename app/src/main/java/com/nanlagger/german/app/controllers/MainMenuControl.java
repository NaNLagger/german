package com.nanlagger.german.app.controllers;

import com.nanlagger.german.app.StartActivity;

/**
 * Created by NaNLagger on 31.12.14.
 *
 * @author Stepan Lyashenko
 */
public class MainMenuControl {
    StartActivity activity;

    public MainMenuControl(StartActivity activity) {
        this.activity = activity;
    }

    public void startTraining() {

    }

    public void startDictionary() {

    }

    public void startProfile() {

    }
}
