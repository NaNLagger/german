package com.nanlagger.german.app.fragments.trainings.translateword;

import android.support.v4.app.Fragment;
import com.nanlagger.german.app.entities.Training;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.trainings.TrainingController;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Stepan on 25.02.2016.
 */
public class TranslateWordController extends TrainingController {

    private static TranslateWordController instance = null;

    private TranslateWordController() {
        idTraining = Training.TRANSLATE_WORD.getIdTraining();
    }

    public static TranslateWordController getInstance() {
        if(instance == null)
            instance = new TranslateWordController();
        return instance;
    }

    @Override
    public void generateTest() {
        super.generateTest();

        long unixTime = System.currentTimeMillis() / 1000L;
        ArrayList<Word> rightW = new ArrayList<>(Word.find(Word.class,
                "PROGRESS < ? and ID NOT IN (SELECT ID_WORD FROM trainings WHERE ID_TRAINING = ? and TIME > ?)",
                new String[]{
                        Word.MAX_PROGRESS+"",
                        Training.TRANSLATE_WORD.getIdTraining()+"",
                        (unixTime - Training.TRANSLATE_WORD.getTimeout())+""
                }));
        Random random = new Random();
        for (int i = 0; i < 10 && rightW.size() > 0; i++) {
            BlockWords blockWords;

            int z = Math.abs(random.nextInt()) % rightW.size();
            Word right = rightW.get(z);
            rightW.remove(z);
            blockWords = new BlockWords(right);

            ArrayList<Word> arrayAnswers = new ArrayList<>(Word.find(Word.class,
                    "PROGRESS < ? and POS = ? and ID NOT IN (SELECT ID_WORD FROM trainings WHERE ID_TRAINING = ? and TIME > ?) LIMIT ?",
                    new String[]{
                            (Word.MAX_PROGRESS + 1) + "",
                            right.getPos(),
                            Training.TRANSLATE_WORD.getIdTraining() + "",
                            (unixTime - Training.TRANSLATE_WORD.getTimeout()) + "",
                            400+""
                    }));
            for (int j = 0; j < 4 && arrayAnswers.size() > 0; j++) {
                int index = Math.abs(random.nextInt()) % arrayAnswers.size();
                blockWords.addAnswer(arrayAnswers.get(index));
                arrayAnswers.remove(index);
            }
            blocks.add(blockWords);
        }
    }

    @Override
    public Fragment getNewInstanceFragment(int position) {
        return TranslateWordSlideFragment.getNewInstance(position);
    }
}
