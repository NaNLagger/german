package com.nanlagger.german.app.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.entities.Word;

/**
 * Created by NaNLagger on 14.01.15.
 *
 * @author Stepan Lyashenko
 */
public class EditWordDialog extends DialogFragment {

    private static final String EXTRA_ID = "id";
    private static final String EXTRA_WORD = "word";
    private NoticeDialogListener mListener;
    private EditText nameEditText;
    private EditText translateEditText;
    private Spinner posSpinner;
    private Spinner typeSpinner;
    private TableRow genRow;
    private Word currentWord;

    public EditWordDialog() {

    }

    public static EditWordDialog newInstance(Long idWord)
    {
        EditWordDialog f = new EditWordDialog();
        Bundle bdl = new Bundle(1);
        bdl.putLong(EXTRA_ID, idWord);
        f.setArguments(bdl);
        return f;
    }

    public static EditWordDialog newInstance(Word word) {
        EditWordDialog f = new EditWordDialog();
        Bundle bdl = new Bundle(1);
        bdl.putParcelable(EXTRA_WORD, word);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_edit, null);

        nameEditText = (EditText) view.findViewById(R.id.nameEditText);
        translateEditText = (EditText) view.findViewById(R.id.translateEditText);
        posSpinner = (Spinner) view.findViewById(R.id.posSpinner);
        typeSpinner = (Spinner) view.findViewById(R.id.typeSpinner);
        genRow = (TableRow) view.findViewById(R.id.genRow);

        currentWord = getWordFromBundle();

        nameEditText.setText(currentWord.getName());
        translateEditText.setText(currentWord.getTranslate());

        Word.Pos[] posValues = Word.Pos.values();
        String[] posValuesString = new String[posValues.length];
        for (int i = 0; i < posValues.length; i++) {
            posValuesString[i] = posValues[i].getPos();
        }
        final ArrayAdapter<String> posAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, posValuesString);
        posSpinner.setAdapter(posAdapter);
        posSpinner.setSelection(posAdapter.getPosition(currentWord.getPos()));

        Word.Gen[] genValues = Word.Gen.values();
        String[] genValuesString = new String[genValues.length];
        for (int i = 0; i < genValues.length; i++) {
            genValuesString[i] = genValues[i].getGen();
        }
        final ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, genValuesString);
        typeSpinner.setAdapter(typeAdapter);
        int position = currentWord.getGen() == null ? 0 : typeAdapter.getPosition(currentWord.getGen().getGen());
        typeSpinner.setSelection(position);
        if(currentWord.getPos() != null && currentWord.getPos().equalsIgnoreCase(Word.Pos.NOUN.getPos())) {
            genRow.setVisibility(View.VISIBLE);
        } else {
            genRow.setVisibility(View.GONE);
        }

        posSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentWord.setPos(posAdapter.getItem(position));
                if(currentWord.getPos().equalsIgnoreCase(Word.Pos.NOUN.getPos())) {
                    genRow.setVisibility(View.VISIBLE);
                } else {
                    genRow.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentWord.setGen(Word.Gen.getValueByGen(typeAdapter.getItem(position)));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setView(view)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        currentWord.setName(nameEditText.getText().toString());
                        currentWord.setTranslate(translateEditText.getText().toString());
                        currentWord.save();
                        mListener.onDialogPositiveClick(EditWordDialog.this);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditWordDialog.this.getDialog().cancel();
                    }
                });
        String title = "";
        if(getTag().equals("add")) {
            title = getResources().getString(R.string.title_add_dialog);
        } else {
            title = getResources().getString(R.string.title_edit_dialog) + ": \"" + currentWord.getName() + "\"";
        }
        builder.setTitle(title);
        return builder.create();
    }

    public void setNoticeDialogListener(NoticeDialogListener listener) {
        mListener = listener;
    }

    private Word getWordFromBundle() {
        Word word = getArguments().getParcelable(EXTRA_WORD);
        if(word != null) {
            return word;
        }
        word = Word.findById(Word.class, getArguments().getLong(EXTRA_ID));
        return word;
    }
}
