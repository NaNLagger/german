package com.nanlagger.german.app.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.entities.Word;

import java.util.ArrayList;

/**
 * Created by NaNLagger on 14.01.15.
 *
 * @author Stepan Lyashenko
 */
public class WordsAdapter extends BaseAdapter {

    ArrayList<Word> entity = null;
    Context context;
    LayoutInflater lInflater;

    public WordsAdapter(Context context, ArrayList<Word> entity) {
        this.entity = entity;
        this.context = context;
        lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return entity.size();
    }

    @Override
    public Object getItem(int position) {
        return entity.get(position);
    }

    @Override
    public long getItemId(int position) {
        Long id = entity.get(position).getId();
        if(id == null)
            id = 0l;
        return id;
    }

    public Word getWord(int position) {
        return ((Word) getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return convertView;
    }
}
