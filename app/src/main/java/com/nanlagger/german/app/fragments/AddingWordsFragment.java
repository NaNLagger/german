package com.nanlagger.german.app.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.nanlagger.german.app.R;
import com.nanlagger.german.app.StartActivity;
import com.nanlagger.german.app.adapters.AddingWordsAdapter;
import com.nanlagger.german.app.controllers.AddingWordsControl;
import com.nanlagger.german.app.controllers.AsyncListener;
import com.nanlagger.german.app.entities.Word;
import com.nanlagger.german.app.fragments.dialogs.EditWordDialog;
import com.nanlagger.german.app.fragments.dialogs.NoticeDialogListener;
import com.nanlagger.german.app.untils.AsyncDictYandexLoader;

import java.util.ArrayList;

/**
 * Created by NaNLagger on 13.01.15.
 *
 * @author Stepan Lyashenko
 */
public class AddingWordsFragment extends Fragment implements AsyncListener, NoticeDialogListener {

    private final String EXTRA_OFFLINE_LIST = "offlineList";
    private final String EXTRA_ONLINE_LIST = "onlineList";

    private AddingWordsControl controller = null;
    private ArrayList<Word> words = new ArrayList<Word>();
    private ArrayList<Word> onlineWords = new ArrayList<Word>();

    private AddingWordsAdapter onlineAdapter;
    private AddingWordsAdapter offlineAdapter;

    /*Init Views*/
    private TextView offlineNoSearchRes;
    private LinearLayout offlineLinearLayout;
    private ListView offlineListView;

    private TextView onlineSearchIndicator;
    private LinearLayout onlineLinearLayout;
    private ListView onlineListView;
    private TextView onlineNoSearchRes;

    public AddingWordsFragment() {
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.addingword_fragment_layout, container, false);

        controller = new AddingWordsControl((StartActivity) getActivity());

        /*Init view offline*/
        offlineLinearLayout = (LinearLayout) rootView.findViewById(R.id.offlineLinearLayout);
        offlineNoSearchRes = (TextView) rootView.findViewById(R.id.offlineNoResTextView);
        offlineListView = (ListView) rootView.findViewById(R.id.offlineListView);
        offlineListView.setFocusable(false);
        offlineAdapter = new AddingWordsAdapter(getActivity(), words);
        offlineAdapter.notifyDataSetChanged();
        offlineListView.setAdapter(offlineAdapter);
        offlineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        /*Init views online*/
        onlineNoSearchRes = (TextView) rootView.findViewById(R.id.onlineNoResTextView);
        onlineSearchIndicator = (TextView) rootView.findViewById(R.id.beginSearchTextView);
        onlineLinearLayout = (LinearLayout) rootView.findViewById(R.id.onlineLinearLayout);
        onlineListView = (ListView) rootView.findViewById(R.id.onlineListView);
        onlineListView.setFocusable(false);
        onlineAdapter = new AddingWordsAdapter(getActivity(), onlineWords);
        onlineAdapter.notifyDataSetChanged();
        onlineListView.setAdapter(onlineAdapter);
        onlineListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });

        /*Init search view*/
        final EditText searchEditText = (EditText) rootView.findViewById(R.id.searchEditText);
        ImageButton searchButton = (ImageButton) rootView.findViewById(R.id.searchImageButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchOfflineWords(searchEditText.getText().toString());
                searchOnlineWords(searchEditText.getText().toString());
            }
        });

        /*Init Tabs offline/online*/
        final Button offlineButton = (Button) rootView.findViewById(R.id.offlineButton);
        final Button onlineButton = (Button) rootView.findViewById(R.id.onlineButton);

        offlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                offlineLinearLayout.setVisibility(View.VISIBLE);
                onlineLinearLayout.setVisibility(View.GONE);
                offlineButton.setEnabled(false);
                onlineButton.setEnabled(true);
            }
        });
        onlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onlineLinearLayout.setVisibility(View.VISIBLE);
                offlineLinearLayout.setVisibility(View.GONE);
                offlineButton.setEnabled(true);
                onlineButton.setEnabled(false);
            }
        });

        /*Init addSelf*/
        final Button addSelfButton = (Button) rootView.findViewById(R.id.addSelfWord);
        addSelfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Word word = new Word();
                word.setName(searchEditText.getText().toString());
                editWordAction(word);
            }
        });

        /*Load savedState*/
        if(savedInstanceState != null) {
            ArrayList<Parcelable> arrayList = savedInstanceState.getParcelableArrayList(EXTRA_OFFLINE_LIST);
            if(arrayList != null) {
                for (Parcelable word : arrayList)
                    words.add((Word) word);
                offlineAdapter.notifyDataSetChanged();
            }

            ArrayList<Parcelable> onArrayList = savedInstanceState.getParcelableArrayList(EXTRA_ONLINE_LIST);
            if(onArrayList != null) {
                for (Parcelable word : onArrayList)
                    onlineWords.add((Word) word);
                onlineAdapter.notifyDataSetChanged();
            }
        }

        setHasOptionsMenu(true);
        setupUI(rootView.findViewById(R.id.parent));
        return rootView;
    }

    private void searchOfflineWords(String key) {
        offlineNoSearchRes.setVisibility(View.GONE);
        words.clear();
        words.addAll(controller.changeList(key));
        offlineAdapter.notifyDataSetChanged();
        if(words.isEmpty())
            offlineNoSearchRes.setVisibility(View.VISIBLE);
        else
            offlineNoSearchRes.setVisibility(View.GONE);
    }

    private void searchOnlineWords(String key) {
        onlineNoSearchRes.setVisibility(View.GONE);
        AsyncDictYandexLoader asyncWordLoader = new AsyncDictYandexLoader(AddingWordsFragment.this);
        asyncWordLoader.execute(key);
        onlineSearchIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void getResult(ArrayList<Word> result) {
        onlineWords.clear();
        onlineWords.addAll(result);
        onlineAdapter.notifyDataSetChanged();
        onlineSearchIndicator.setVisibility(View.GONE);
        if(onlineWords.isEmpty())
            onlineNoSearchRes.setVisibility(View.VISIBLE);
        else
            onlineNoSearchRes.setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_OFFLINE_LIST, words);
        outState.putParcelableArrayList(EXTRA_ONLINE_LIST, onlineWords);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActionBar actionBar = ((StartActivity) getActivity()).getSupportActionBar();
        actionBar.setTitle(getString(R.string.title_section_add));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.global, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void setupUI(View view) {
        if(!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(getActivity());
                    return false;
                }

            });
        }

        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    private void editWordAction(Word word) {
        EditWordDialog dialog = EditWordDialog.newInstance(word);
        dialog.setNoticeDialogListener(this);
        dialog.show(getFragmentManager(), "add");
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
